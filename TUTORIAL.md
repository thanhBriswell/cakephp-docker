## DOCKER

### Create image

```
docker build -t container_name .
```

### Start docker

```
docker-compose up
```

- Docker tạo db cho db server hơi chậm. cần chờ tầm 5 phút

### Down container docker

```
docker-compose down
```

### Docker compose command line

https://docs.docker.com/compose/reference/

### Docker compose file version

https://docs.docker.com/compose/compose-file/compose-file-v2/


### depend on

https://docs.docker.com/compose/compose-file/compose-file-v3/#depends_on


### env file

https://docs.docker.com/compose/compose-file/compose-file-v3/#env_file

### Get Docker IP

https://stackjava.com/docker/huong-dan-lay-dia-chi-ip-cua-docker-container.html

### Docker network

https://runnable.com/docker/docker-compose-networking

## Docker in WSL

- Go to Windows drive:

`cd /mnt/e`

`e` is E drive of Windows

#### Link referrence

https://www.tutorialspoint.com/docker/building_web_server_docker_file.htm

https://www.digitalocean.com/community/tutorials/apache-configuration-error-ah00558-could-not-reliably-determine-the-server-s-fully-qualified-domain-name

https://stackoverflow.com/questions/28411249/edit-conf-file-in-docker-file/28870123

https://topdev.vn/blog/20-truong-hop-su-dung-lenh-docker-cho-developer/

https://blog.tomosia.com/cakephp-lan-dau-lam-chuyen-day-voi-cakephp-tren-moi-truong-docker/

https://github.com/diepz/cakephp

https://xuanthulab.net/lenh-docker-compose-tao-va-chay-cac-dich-vu-docker.html


### Setup Nodejs in WSL

https://docs.microsoft.com/en-us/windows/nodejs/setup-on-wsl2